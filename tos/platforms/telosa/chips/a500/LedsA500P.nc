/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/** 
 * Implementation of interface for contorlling LED on the CCSR SmartBox
 *
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @author Alex Gluhak <A.Gluhak@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/

module LedsA500P @safe() {

    provides interface LedsA500 as Leds;
	uses {
		// Defined in tos/chips/msp430/pins/HplMsp430GeneralIO.nc
		interface HplMsp430GeneralIO as Port;
	}
	
}

implementation {
    
    norace bool on = FALSE;

    async command void Leds.ledOn() {
        call Port.set();
        on = TRUE;
    }
    
    async command void Leds.ledOff() {
        call Port.clr();
        on = FALSE;
    }
    
    async command void Leds.ledToggle() {
       if ( on == TRUE )
            call Port.clr();
        else call Port.set();
        on = !on;
}
}


