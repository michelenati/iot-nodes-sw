/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Module implementing the EnergyMeter interface
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/


#include "EnergyData.h"
#ifdef PM_DEBUG
#include "printf.h"
#endif

module PloggP {

    provides {
        interface SplitControl;
        interface EnergyMeter;
    }

    uses {
        interface Boot;
        interface Leds;
        interface StdControl as UartPloggStdControl;
        interface UartStream as UartPloggStream;
        interface Resource as UartResource;
        interface Timer<TMilli> as TimerLog;
        interface Timer<TMilli> as TimerConnected;
    }
}

implementation {

    //
    // Constants
    //

    /** Relevant ASCII values when scanning the raw reading. */
    enum {
        ASCII_CR   = 0x0d,    //Carriage return
        ASCII_SP   = 0x20,    //Space
        ASCII_0    = 0x30,    //Zero
        ASCII_LF   = 0x0a,    //Line feed
        ASCII_P    = 0x2e,    //Period
        ASCII_M    = 0x2d,    //Minus
    };

    enum {
        ATE0_COMMAND_LENGTH   = 4,
        ATI_COMMAND_LENGTH    = 3,
        ATS0_COMMAND_LENGTH   = 6,
        ATS512_COMMAND_LENGTH = 8,
        ATS502_COMMAND_LENGTH = 8,
        ATS536_COMMAND_LENGTH = 8,
        ATW_COMMAND_LENGTH    = 4,
        ATZ_COMMAND_LENGTH    = 3,
        HELP_COMMAND_LENGTH   = 4,
    };
    
    enum {
        DEVICE_TYPE_LEN       = 30,
        RESP_OK_LEN           = 6,
        TIMER_RESP_LEN        = 5,
        SET_TIMER_RESP_LEN    = 18,
        CONN_COMMAND_LEN      = 12,
        PLOGG_COMMAND_LEN     = 2,
        RX_COMMAND_LEN        = 3,
#ifdef PM_OLD_PLOGG
        BOOT_SEQUENCE_LEN     = 7,
#else
        BOOT_SEQUENCE_LEN     = 8,
#endif
        LOG_MSG_LEN           = 53,
        FIXED_HEADER_LEN      = 2
    };
    
    // These should be more properly constant
    // MN: in case of old plogg consider to add a costant offset of 20
#ifndef PM_OLD_PLOGG
    // command ate0
    uint8_t arrayCommandATE0[] = { 0x41, 0x54, 0x45, 0x30 };
    // command ATI
    uint8_t arrayCommandATI[] = { 0x41, 0x54, 0x49 };
    // command ATS0=1
    uint8_t arrayCommandATS0[] = { 0x41, 0x54, 0x53, 0x30, 0x3d, 0x31 };
    // command ATS512=4
    uint8_t arrayCommandATS512[] = { 0x41, 0x54, 0x53, 0x35, 0x31, 0x32, 0x3d, 0x34 };
    // command ATS502=0
    uint8_t arrayCommandATS502[] = { 0x41, 0x54, 0x53, 0x35, 0x30, 0x32, 0x3d, 0x30 };
    // command ATS536=1
    uint8_t arrayCommandATS536[] = { 0x41, 0x54, 0x53, 0x35, 0x33, 0x36, 0x3d, 0x31 };
    // command AT&W
    uint8_t arrayCommandATW[] = { 0x41, 0x54, 0x26, 0x57 };
    // command ATZ
    uint8_t arrayCommandATZ[] = { 0x41, 0x54, 0x5a };
#else
    // command ati
    uint8_t arrayCommandATI[] = { 0x61, 0x74, 0x69 };
    // command ats0=1
    uint8_t arrayCommandATS0[] = { 0x61, 0x74, 0x73, 0x30, 0x3d, 0x31 };
    // command ats512=4
    uint8_t arrayCommandATS512[] = { 0x61, 0x74, 0x73, 0x35, 0x31, 0x32, 0x3d, 0x34 };
    // command ats502=0
    uint8_t arrayCommandATS502[] = { 0x61, 0x74, 0x73, 0x35, 0x30, 0x32, 0x3d, 0x30 };
    // command ats536=1
    uint8_t arrayCommandATS536[] = { 0x61, 0x74, 0x73, 0x35, 0x33, 0x36, 0x3d, 0x31 };
    // command at&w
    uint8_t arrayCommandATW[] = { 0x61, 0x74, 0x26, 0x77 };
    // command atz
    uint8_t arrayCommandATZ[] = { 0x61, 0x74, 0x7a };
#endif
    // command "CONNECT "
    uint8_t arrayCommandCONN[] = { ASCII_CR, ASCII_LF, 0x43, 0x4f, 0x4e, 0x4e, 0x45, 0x43, 0x54, ASCII_SP, ASCII_CR, ASCII_LF };
    // command YV
    uint8_t arrayCommandYV[] = { 0x59, 0x56 };
    // command YS
    uint8_t arrayCommandYS[] = { 0x59, 0x53 };
    // command YM
    uint8_t arrayCommandYM[] = { 0x59, 0x4d };
    // command YL4
    uint8_t arrayCommandYL[] = { 0x59, 0x4c, 0x34 };
    // command YN
    uint8_t arrayCommandYN[] = { 0x59, 0x4e };
    // command YR
    uint8_t arrayCommandYR[] = { 0x59, 0x52 };
    // command YI
    uint8_t arrayCommandYI[] = { 0x59, 0x49, ASCII_SP, 0x30, 0x30, 0x30, 0x30 };
    // command YX
    uint8_t arrayCommandYX[] = { 0x59, 0x58 };
    // command YD_R
    uint8_t arrayCommandYDR[] = { 0x59, 0x44, ASCII_SP, 0x31 };
    // command YD
    uint8_t arrayCommandYD[] = { 0x59, 0x44, ASCII_SP, 0x30 };
    // command YN_name
    uint8_t arrayCommandYNN[] = { 0x59, 0x4e, ASCII_SP, 0x4d, 0x69, 0x63, 0x68, 0x65, 0x6c, 0x65 };
    // command HELP
    uint8_t arrayCommandHELP[] = { 0x48, 0x45, 0x4c, 0x50 };
    // command RTD
    uint8_t arrayCommandRTD[] = { 0x72, 0x74, 0x64, 0x30, 0x30, ASCII_P, 0x30, 0x30, ASCII_P, 0x30, 0x30, ASCII_CR, ASCII_LF, ASCII_CR, ASCII_LF };
    // command RTT
    uint8_t arrayCommandRTT[] = { 0x72, 0x74, 0x74, 0x30, 0x30, ASCII_P, 0x30, 0x30, ASCII_P, 0x30, 0x30, ASCII_CR, ASCII_LF, ASCII_CR, ASCII_LF };
    // command YE
    uint8_t arrayCommandYE[] = { 0x59, 0x45, ASCII_SP, 0x31 };
    uint8_t arrayCommandYED[] = { 0x59, 0x45, ASCII_SP, 0x30 };
    // command YO_C
    uint8_t arrayCommandYO_C[] = { 0x59, 0x4f };
    // command YO
    uint8_t arrayCommandYO[] = { 0x59, 0x4f, ASCII_SP, 0x30, ASCII_SP, 0x30, 0x30, ASCII_SP, 0x30, 0x30 };
    
    
    // Device Type: Ezurio blu2i Module II - correctly embedded in AT command
    uint8_t deviceType[] = { ASCII_CR, ASCII_LF, 0x45, 0x7a, 0x75, 0x72, 0x69, 0x6f, ASCII_SP, 0x62, 0x6c, 0x75, 0x32, 0x69, ASCII_SP, 0x4d, 0x6f, 0x64, 0x75, 0x6c, 0x65, ASCII_SP, 0x49, 0x49, ASCII_CR, ASCII_LF, 0x4F, 0x4B, ASCII_CR, ASCII_LF };
    // Response OK
    uint8_t responseOK[] = { ASCII_CR, ASCII_LF, 0x4F, 0x4B, ASCII_CR, ASCII_LF };

#ifdef PM_DEBUG
    uint8_t         turn_on;
#endif
    uint8_t         commandID;
    uint16_t        sampleInterval;
    norace bool     noResponse;
    norace bool     sendingCommand;
    norace bool     connected;
    norace bool     detectedCommand;
    norace bool     receivedResponse;
    norace uint8_t  readSymbols;
    norace uint8_t  sentByte;
    norace uint8_t* buffer[BOOT_SEQUENCE_LEN+1];
    norace uint8_t* resp[2];
    norace uint8_t  buffer_len[BOOT_SEQUENCE_LEN+1];
    norace uint8_t  resp_len[2];
    norace uint8_t* toSend;
    norace uint8_t  toSendLen;
    
    uint8_t         buffer_resp[LOG_MSG_LEN+FIXED_HEADER_LEN];
    norace uint8_t  currentLen;
    norace uint8_t  expectedLen;
    
    task void startDone();
    task void sendConnect();
    task void getLiveInfo();
#ifdef PM_DEBUG
    task void flushBuffer();
#endif

    void _error_( uint8_t v ) {
		call Leds.set( v );
		for (;;) {}		
	}
	
	int convert_int( int value ) {
	   if ( value % 10 != 0 )
	       value = value / 10 + ( value % 10 ) * 10;
	       
	   return value;
	}

    task void releaseUartResource() {
        
        call UartPloggStream.disableReceiveInterrupt();
        if ( call UartResource.release() != SUCCESS )
            post releaseUartResource();
        else {
            if ( receivedResponse == TRUE ) {
                signal EnergyMeter.receiveDone( &buffer_resp[0], expectedLen, SUCCESS );
                receivedResponse = FALSE;
            }
        }
    }

    task void startDone() {
        signal SplitControl.startDone( SUCCESS );
    }

    task void sendConnect() {
        toSendLen = CONN_COMMAND_LEN;
        toSend = &arrayCommandCONN[0];
        if ( call UartPloggStream.send( (toSend+sentByte), 1 ) == SUCCESS ) {
            sendingCommand = FALSE;
        } else post sendConnect();
    }
    
    task void getLiveInfo() {
        if ( call EnergyMeter.getLiveInfo() == FAIL )
            post getLiveInfo();
        else call TimerLog.startOneShot( sampleInterval );
    }
    
#ifdef PM_DEBUG
    task void flushBuffer() {
        printfflush();
    }
#endif
    
    command error_t SplitControl.start() {
        //MN: to check - maybe it is better to return this signal after the boot sequence
        /*
        if ( connected == TRUE )
            signal SplitControl.startDone( SUCCESS );
        else post releaseUartResource(); //MN: for nodes not connected to any PLOGG
        */
        return SUCCESS;
    }
    
    command error_t SplitControl.stop() {
        signal SplitControl.stopDone( SUCCESS );
        return SUCCESS;
    }
    
    
    event void Boot.booted() {
#ifdef PM_DEBUG
        turn_on             = 0;
#endif
        currentLen          = 0;
        expectedLen         = 0;
        sampleInterval      = 0;
        detectedCommand     = FALSE;
        receivedResponse    = FALSE;
        atomic commandID    = 0;
        connected           = FALSE;
        sendingCommand      = FALSE;
        noResponse          = FALSE;
        
#ifdef PM_OLD_PLOGG
        buffer[0]           = &arrayCommandATI[0];
        buffer[1]           = &arrayCommandATS0[0];
        buffer[2]           = &arrayCommandATS512[0];
        buffer[3]           = &arrayCommandATS502[0];
        buffer[4]           = &arrayCommandATS536[0];
        buffer[5]           = &arrayCommandATW[0];
        buffer[6]           = &arrayCommandATZ[0];
        buffer[7]           = &arrayCommandHELP[0];
        buffer_len[0]       = ATI_COMMAND_LENGTH;
        buffer_len[1]       = ATS0_COMMAND_LENGTH;
        buffer_len[2]       = ATS512_COMMAND_LENGTH;
        buffer_len[3]       = ATS502_COMMAND_LENGTH;
        buffer_len[4]       = ATS536_COMMAND_LENGTH;
        buffer_len[5]       = ATW_COMMAND_LENGTH;
        buffer_len[6]       = ATZ_COMMAND_LENGTH;
        buffer_len[7]       = HELP_COMMAND_LENGTH;
#else
        buffer[0]           = &arrayCommandATE0[0];
        buffer[1]           = &arrayCommandATI[0];
        buffer[2]           = &arrayCommandATS0[0];
        buffer[3]           = &arrayCommandATS512[0];
        buffer[4]           = &arrayCommandATS502[0];
        buffer[5]           = &arrayCommandATS536[0];
        buffer[6]           = &arrayCommandATW[0];
        buffer[7]           = &arrayCommandATZ[0];
        buffer[8]           = &arrayCommandHELP[0];
        buffer_len[0]       = ATE0_COMMAND_LENGTH;
        buffer_len[1]       = ATI_COMMAND_LENGTH;
        buffer_len[2]       = ATS0_COMMAND_LENGTH;
        buffer_len[3]       = ATS512_COMMAND_LENGTH;
        buffer_len[4]       = ATS502_COMMAND_LENGTH;
        buffer_len[5]       = ATS536_COMMAND_LENGTH;
        buffer_len[6]       = ATW_COMMAND_LENGTH;
        buffer_len[7]       = ATZ_COMMAND_LENGTH;
        buffer_len[8]       = HELP_COMMAND_LENGTH;
#endif
        resp[0]             = &deviceType[0];
        resp[1]             = &responseOK[0];
        resp_len[0]         = DEVICE_TYPE_LEN;
        resp_len[1]         = RESP_OK_LEN;
        sentByte            = 0;
        
        call UartPloggStdControl.start();
        call Leds.led0On();
#ifdef PM_DEBUG
        call TimerLog.startOneShot( 10000 );
#endif

#ifndef PM_PLOGG_CONNECTED
        post releaseUartResource();
        post startDone();
#endif

#ifdef PM_AUTO_RESET
        call TimerConnected.startOneShot( PM_CONNECTION_DELAY );
#endif

    }
    
    async event void UartPloggStream.receivedByte(uint8_t byte) {
        uint8_t respID;
        call Leds.led0Toggle();
#ifdef PM_DEBUG
        printf("%u %c\n", byte, byte);
#endif

        if ( connected == FALSE ) {        
            if ( byte == ASCII_CR ) {
                if ( detectedCommand == TRUE ) {
#ifdef PM_OLD_PLOGG
                    ( commandID == 0 ) ? ( respID = commandID ) : ( respID = 1 );
#else
                    ( commandID == 1 ) ? ( respID = 0 ) : ( respID = 1 );
#endif
                    toSendLen = resp_len[respID];
                    toSend = resp[respID]; 
                    call UartPloggStream.send( (toSend+sentByte), 1 );
                    detectedCommand = FALSE;
                    readSymbols = 0;
                }
            } else {
                if ( byte == *(buffer[commandID]+readSymbols) ) {
                    readSymbols ++;
                    if ( readSymbols == buffer_len[commandID] ) {
                        detectedCommand = TRUE;
                    }
                }
            }
        } else {
            buffer_resp[currentLen] = byte;
            currentLen ++;
            if ( currentLen == expectedLen ) {
                currentLen = 0;
                //MN: command response received
                receivedResponse = TRUE;
                if ( commandID >= ( BOOT_SEQUENCE_LEN + 1 ) ) post releaseUartResource();
#ifdef PM_DEBUG
                printf("DONE\n");
#endif
            } else {
                if ( byte == *(buffer[BOOT_SEQUENCE_LEN]+readSymbols) ) {
                    readSymbols ++;
                    if ( readSymbols == buffer_len[BOOT_SEQUENCE_LEN] ) {
                        readSymbols = 0;
                        currentLen = 0;
                        post releaseUartResource();
                    }
                }
            }
        }
#ifdef PM_DEBUG
        //post flushBuffer();
#endif
    }
    
    async event void UartPloggStream.sendDone(uint8_t* buf, uint16_t len, error_t error) {
    
        if ( error == SUCCESS )
            sentByte ++;
        if ( sentByte < toSendLen ) {
            atomic {
                if ( call UartPloggStream.send( (toSend+sentByte), 1 ) == SUCCESS ) {
                    if ( ( sentByte + 1 ) == toSendLen ) {
                        if ( commandID <= ( BOOT_SEQUENCE_LEN + 1 ) ) commandID ++;
                    }
                }
            }
        } else {
            call Leds.led2Toggle();
            sentByte = 0;
            if ( ( commandID == BOOT_SEQUENCE_LEN ) || ( sendingCommand == TRUE ) ) {
                connected = TRUE;
                //post startDone();
                post sendConnect();
            }
            //MN: connection established
            if ( ( commandID == ( BOOT_SEQUENCE_LEN + 1 ) ) && ( toSend = &arrayCommandCONN[0] ) ) {
                post releaseUartResource();
                post startDone();
            }
            if ( noResponse == TRUE ) {
                noResponse = FALSE;
                receivedResponse = TRUE;
                post releaseUartResource();
            }
        }
     }
    
    async event void UartPloggStream.receiveDone(uint8_t* buf, uint16_t len, error_t error) {}

    event void TimerLog.fired() {
#ifdef PM_DEBUG
        post flushBuffer();
        //call EnergyMeter.setRTD( 10, 10, 10 );
        //call EnergyMeter.getLiveInfo();
        //call EnergyMeter.enableOffTimers( 1 );

        turn_on ++;
        call EnergyMeter.enableOffTimers( turn_on % 2 );
        //call EnergyMeter.setOffTimer(2, 11, 22, 11, 33);
        //call EnergyMeter.setOffTimer(0, 0, 0, 0, 0);
        //call EnergyMeter.getMaxValues();
        //call EnergyMeter.resetAccumulators();
        //call EnergyMeter.setLoggingInterval(1);
        //call EnergyMeter.deleteLoggedValues();
        //call EnergyMeter.getLoggedValues(0); //MN: TO CHECK
        call TimerLog.startOneShot( 5000 );
#else
        post getLiveInfo();
#endif
    }
    
    event void TimerConnected.fired() {
        if ( connected == FALSE ) {
            connected = TRUE;
            atomic commandID = BOOT_SEQUENCE_LEN + 1;
            post releaseUartResource();
            post startDone();
        }
    }
    
    event void UartResource.granted(){
    
        call UartPloggStream.enableReceiveInterrupt();
        if ( call UartPloggStream.send( (toSend+sentByte), 1 ) == SUCCESS )
            sendingCommand = TRUE;
    }


    error_t sendCommand() {
    
        if ( sendingCommand == TRUE )
            return FAIL;

        if ( connected == TRUE ) {
            readSymbols = 0;
            if ( call UartResource.request() == SUCCESS ) {
                //sendingCommand = TRUE;
                return SUCCESS;
            }
        }
        return FAIL;
    }

    command error_t EnergyMeter.getPeriodicValues( uint16_t interval ) {

        if ( sendingCommand == TRUE )
            return FAIL;
            
        if ( connected == TRUE ) {
            sampleInterval = interval;
            call TimerLog.stop();
            //call TimerLog.startPeriodic( interval );
            call TimerLog.startOneShot( sampleInterval );
            return SUCCESS;   
        }
        
        return FAIL;
    }

    command error_t EnergyMeter.resetAccumulators() {
        toSendLen = PLOGG_COMMAND_LEN;
        toSend = &arrayCommandYR[0];
        expectedLen = 5+FIXED_HEADER_LEN;
        currentLen = 0;
    
        return sendCommand();
    }
    
    command error_t EnergyMeter.setLoggingInterval( uint16_t interval ) {
        char ascii[4];
        toSendLen = PLOGG_COMMAND_LEN+5;
        toSend = &arrayCommandYI[0];
        itoa(interval, ascii, 10);
        arrayCommandYI[3] = ascii[3];
        arrayCommandYI[4] = ascii[2];
        arrayCommandYI[5] = ascii[1];
        arrayCommandYI[6] = ascii[0];
        expectedLen = 5+FIXED_HEADER_LEN;
        currentLen = 0;
        
        return sendCommand();
    }
    
    command error_t EnergyMeter.getLiveInfo() {
        toSendLen = PLOGG_COMMAND_LEN;
        toSend = &arrayCommandYV[0];
        expectedLen = LOG_MSG_LEN+FIXED_HEADER_LEN;
        currentLen = 0;
        
        return sendCommand();
    }
    
    command error_t EnergyMeter.getLoggedValues(uint8_t reset) {
        toSendLen = PLOGG_COMMAND_LEN+2;
        if ( reset == 1 )
            toSend = &arrayCommandYDR[0];
        else toSend = &arrayCommandYD[0]; 
        expectedLen = LOG_MSG_LEN+FIXED_HEADER_LEN;
        currentLen = 0;
        
        return sendCommand();
    }
    
    command error_t EnergyMeter.getMaxValues() {
        toSendLen = PLOGG_COMMAND_LEN;
        toSend = &arrayCommandYM[0];
        expectedLen = 30+FIXED_HEADER_LEN;
        currentLen = 0;
        
        return sendCommand();
    }
    
    command error_t EnergyMeter.setRTD(uint16_t year, uint16_t month, uint16_t day) {
        char a_year[2];
        char a_month[2];
        char a_day[2];
        year = convert_int( year );
        if ( month == 10 )
            month = 24;
        else month = convert_int( month );
        if ( day == 10 )
            day = 24;
        else if ( day == 20 )
            day = 25;
        else if ( day == 30 )
            day = 26;
        else day = convert_int( day );
        //toSendLen = RX_COMMAND_LEN+17;
        toSendLen = RX_COMMAND_LEN+12;
        toSend = &arrayCommandRTD[0];
        itoa(year, a_year, 10);
        arrayCommandRTD[3] = a_year[1];
        arrayCommandRTD[4] = a_year[0];
        itoa(month, a_month, 10);
        arrayCommandRTD[6] = a_month[1];
        arrayCommandRTD[7] = a_month[0];
        itoa(day, a_day, 10);
        arrayCommandRTD[9] = a_day[1];
        arrayCommandRTD[10] = a_day[0];
        expectedLen = 0;
        currentLen = 0;
        noResponse = TRUE;
        
        return sendCommand();
        
    }
    
    command error_t EnergyMeter.setRTT(uint16_t hour, uint16_t minute, uint16_t second) {
        char a_hour[2];
        char a_minute[2];
        char a_second[2];
        if ( hour == 10 )
            hour = 24;
        else if ( hour == 20 )
            hour = 25;
        else hour = convert_int( hour );
        minute = convert_int( minute );
        second = convert_int( second );
        //toSendLen = RX_COMMAND_LEN+17;
        toSendLen = RX_COMMAND_LEN+12;
        toSend = &arrayCommandRTT[0];
        itoa(hour, a_hour, 10);
        arrayCommandRTT[3] = a_hour[1];
        arrayCommandRTT[4] = a_hour[0];
        itoa(minute, a_minute, 10);
        arrayCommandRTT[6] = a_minute[1];
        arrayCommandRTT[7] = a_minute[0];
        itoa(second, a_second, 10);
        arrayCommandRTT[9] = a_second[1];
        arrayCommandRTT[10] = a_second[0];
        expectedLen = 0;
        currentLen = 0;
        noResponse = TRUE;
        
        return sendCommand();
        
    }
    
    command error_t EnergyMeter.enableOffTimers(uint8_t enable) {
        toSendLen = PLOGG_COMMAND_LEN+2;
        if ( enable == 1 )
            toSend = &arrayCommandYE[0];
        else toSend = &arrayCommandYED[0]; 
        expectedLen = TIMER_RESP_LEN+FIXED_HEADER_LEN;
        currentLen = 0;

        return sendCommand();
    }
    
    command error_t EnergyMeter.setOffTimer(uint8_t timer_id, uint8_t hour_on, uint8_t minute_on, uint8_t hour_off, uint8_t minute_off) {
        char convert_id[1];
        char a_hour[1];
        char a_minute[1];
        toSendLen = PLOGG_COMMAND_LEN;
        if ( timer_id >= 4 )
            toSend = &arrayCommandYO_C[0];
        else {
            toSend = &arrayCommandYO[0];
            itoa(hour_on, a_hour, 10);
            itoa(minute_on, a_minute, 10);
            arrayCommandYO[5] = a_hour[0];
            arrayCommandYO[6] = a_minute[0];
            itoa(hour_off, a_hour, 10);
            itoa(minute_off, a_minute, 10);
            arrayCommandYO[8] = a_hour[0];
            arrayCommandYO[9] = a_minute[0];
            itoa(timer_id, convert_id, 10);
            arrayCommandYO[3] = convert_id[0];    
            toSendLen += 7;
        }
        expectedLen = SET_TIMER_RESP_LEN+FIXED_HEADER_LEN;
        currentLen = 0;

        return sendCommand();
    }
    
    command error_t EnergyMeter.deleteLoggedValues() {
        toSendLen = PLOGG_COMMAND_LEN;
        toSend = &arrayCommandYX[0];
        expectedLen = 5+FIXED_HEADER_LEN;
        currentLen = 0;
        
        return sendCommand();
    }
    
    default async event void EnergyMeter.receiveDone( uint8_t* buf, uint8_t len, error_t error ) {}
    default event void SplitControl.startDone( error_t error ) {}

}

