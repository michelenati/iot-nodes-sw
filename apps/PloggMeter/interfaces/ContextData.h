/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Message for returning CCSR SmartBox values to calling modules
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/


#ifndef _CONTEXT_DATA_H
#define _CONTEXT_DATA_H

#define	SB_MAX_SENSOR_VALUE	4095
#define	CS_SENSING_PERIOD	1500
#define	CS_MAX_METRICS		6

typedef nx_struct context_msg {
	
	nx_uint8_t	PIR;
	nx_uint8_t	vib;
	nx_uint16_t	mic;
	nx_uint16_t	temp;
	nx_uint16_t	light;
#ifdef PM_WISEBED_PKT
	nx_uint16_t	source;
#endif
	nx_uint16_t	packet_id;
	nx_uint32_t	watt;
	nx_uint32_t	frequency;
	nx_uint32_t	rms_voltage;
	nx_uint32_t	rms_current;
	nx_uint32_t	plogg_on_time;
	nx_uint32_t	reactive_power;
	nx_uint32_t	phase_angle;
	nx_uint32_t	time_on;
} context_msg_t;

enum {
	AM_CONTEXT_MSG			= 122,
};

#endif
