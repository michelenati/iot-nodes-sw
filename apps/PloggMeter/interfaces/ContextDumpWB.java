// $Id$

/*									tab:4
 * "Copyright (c) 2000-2003 The Regents of the University  of California.  
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice, the following
 * two paragraphs and the author appear in all copies of this software.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 * OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 * CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 * Copyright (c) 2002-2003 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE     
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA, 
 * 94704.  Attention:  Intel License Inquiry.
 */

import java.util.*;
import java.text.*;
import java.io.*;

import net.tinyos.packet.*;
import net.tinyos.util.*;
import net.tinyos.message.*;

public class ContextDumpWB {
	
	// Print out number, so 123456,6,3 will look likne 123.456
	//
	static String printNumber (String str, int d, int c, int p, boolean trailingZeros)
	{
		boolean nothingPrinted=true;
		short i;
		short[] digits = new short[16];
		boolean suppressLeadingZeros=!trailingZeros;
		
		if (d < 0)
		{
			d = -d;
		}
		
		// work out the number backwards to start with
		for (i = 0; i < c; i++)
		{                                   
			digits[(c-1)-i] = (short)(d%10);
			d /= 10;
		}
		
		// print the number forwards now..
		for (i = 0; i < c; i++)
		{      
			if ( suppressLeadingZeros && digits[i] == 0)
			{
				// skip leading zeros
			}
			else
			{
				// print digit
				str += (char) ('0' + digits[i]);
				
				// clear the leading zero suppressor now
				suppressLeadingZeros = false;
				nothingPrinted = false;
			}
			
			// decimal point..
			if (((c-1)-i) == p && p != 0)
			{
				// still suppressing zeros, then print a '0' else it'll look like .76 instead of 0.76
				if (suppressLeadingZeros)
				{
					suppressLeadingZeros=false;
					str += "0.";
				}
				else
					str += ".";
				
				nothingPrinted = false;
			}
		}
		
		// ensure we have at least one 0
		if (nothingPrinted)
			str += "0";
		
		return str;
		
	}
	
	// Given a logging item and value print out nicely to the user
	//public String FormatLogValue(ValueType valueType, DWORD value, BOOL SkipUnits)
	static public String formatLogValue( int valueType, long value )
	{
		String str = new String();
		String g_monthStrings[] = new String[12];
		g_monthStrings[0] = new String("JAN");
		g_monthStrings[1] = new String("FEB");
		g_monthStrings[2] = new String("MAR");
		g_monthStrings[3] = new String("APR");
		g_monthStrings[4] = new String("MAY");
		g_monthStrings[5] = new String("JUN");
		g_monthStrings[6] = new String("JUL");
		g_monthStrings[7] = new String("AUG");
		g_monthStrings[8] = new String("SEP");
		g_monthStrings[9] = new String("OCT");
		g_monthStrings[10] = new String("NOV");
		g_monthStrings[11] = new String("DEC");
		
		switch(valueType)
		{
				//case VAL_TIME:
			case 0:
			{
				//	Real Time Clock	
				//  Year | Month | Day | Hour | Min | Sec
				//    6      4       5      5     6     6
				
				// Note year is only valid from 2000 -> 2063
				//
				long year = (value >> 26);
				long month = ((value >> 22) & 0xf) + 1;
				long day = ((value >> 17) & 0x1f) + 1;
				long hour = ((value >> 12) & 0x1f);
				long min = ((value >> 6) & 0x3f);
				long sec = value & 0x3f;
				
				// Print out as 2007 Feb 22 12:04:04
				str += (char) ('0' + (hour / 10)); 
				str += (char) ('0' + (hour % 10)); 
				str += ":";
				str += (char) ('0' + (min / 10)); 
				str += (char) ('0' + (min % 10)); 
				str += ":";
				str += (char) ('0' + (sec / 10)); 
				str += (char) ('0' + (sec % 10)); 
				str += " ";
				str += (char) ('0' + (day / 10)); 
				str += (char) ('0' + (day % 10)); 
				str += " ";
				str += g_monthStrings[(int)month-1];
				
				str += " ";
				str += "2"; 
				str += (char) ('0' + (year / 100)); 
				str += (char) ('0' + (year / 10 % 10)); 
				str += (char) ('0' + (year % 10)); 
				break;
			}
			case 1:
			{
				// Watts
				
				// is this negative
				if ((value & 0x80000000) == 0x80000000)
				{
					str += "-";
					value &= ~0x80000000;
				}
				
				value /= 10;
				
				// print up to 99Kw
				str = printNumber(str,(int)value,8,2,false) + " W ";
				break;
			}
			case 2:
			{
				// kWh	0.0000 ñ 9,999.9999 kWh
				// print up to 9999kw
				str = printNumber(str,(int)value,10,4,false) + " kWh ";
				break;
			}
			case 3:
			{
				// frequency
				// print up to 9999hz
				str = printNumber(str,(int)value,5,1,false) + " Hz ";
				break;
			}
			case 4:
			{
				// RMS voltage
				value/=100;
				// print up to 999v
				str = printNumber(str,(int)value,6,1,false) + " V ";
				break;
			}
			case 5:
			{
				// RMS current
				if ((value & 0x80000000) == 0x80000000)
				{
					str += "-";
					value &= ~0x80000000;
				}
				
				// print up to 999A
				str = printNumber(str,(int)value,6,3,false) + " A ";
				break;
			}
			case 6:
			{
				// Acc on time
				long days = value / (8640000L);			// 10ms in a day
				long hours = (value / (360000L)) % 24;	// 10ms in a hour
				long minutes = (value / (60*100)) % 60;	// 10ms in a minute
				long seconds = (value / (100)) % 60;	// 10ms in a seconds
				
				
				str = printNumber(str,(int)days,5,0,false) + " days ";
				str = printNumber(str,(int)hours,2,0,true) + ":";
				str = printNumber(str,(int)minutes,2,0,true) + ".";
				str = printNumber(str,(int)seconds,2,0,true);
				break;
				
			}
			case 7:
			{
				// Reactive power
				
				// is this negative
				if ((value & 0x80000000) == 0x80000000)
				{
					str += '-';
					value &= ~0x80000000;
				}
				
				value/=10;
				// print up to 99999var
				str = printNumber(str,(int)value,8,2,false) + " VAR ";
				break;
			}
			case 8:
			{
				// KVARh reactive power
				// print up to 9999999KVar
				str = printNumber(str,(int)value,10,4,false) + " KVARh ";
				break;
			}
			case 9:
			{
				// phase angle
				// print up to 9999 degrees
				str = printNumber(str,(int)value,4,0,false) + " Degrees ";
				break;
			}
			default:
			{
				break;
			}
		}
		
		return str;
	}
	
	
    public static void main(String args[]) throws IOException {
        String source = null;
        PacketSource reader;
        if (args.length == 2 && args[0].equals("-comm")) {
          source = args[1];
        }
	else if (args.length > 0) {
	    System.err.println("usage: java net.tinyos.tools.Listen [-comm PACKETSOURCE]");
	    System.err.println("       (default packet source from MOTECOM environment variable)");
	    System.exit(2);
	}
        if (source == null) {	
  	  reader = BuildSource.makePacketSource();
        }
        else {
  	  reader = BuildSource.makePacketSource(source);
        }
	if (reader == null) {
	    System.err.println("Invalid packet source (check your MOTECOM environment variable)");
	    System.exit(2);
	}

	try {
	  reader.open(PrintStreamMessenger.err);
	  for (;;) {
		  byte[] packet = reader.readPacket();
		  byte[] serialpacket = new byte[ 7 ];
		  byte[] subpacket = new byte[ packet.length - 8 ];
		  
		  System.arraycopy( packet, 1, serialpacket, 0, 7 );
		  System.arraycopy( packet, 8, subpacket, 0, packet.length - 8 );
		  
		  ContextMsgWB    msg = new ContextMsgWB( subpacket );
		  SerialPacket serial = new SerialPacket( serialpacket );
		  
		  SimpleDateFormat sdf = new SimpleDateFormat( "mm:ss:SS" );
		  System.err.print( sdf.format( new Date() ) + "\t" );
		  
		  //System.out.println( serial.toString() );
		  
		  System.out.println(new Date().toString()+" "+msg.get_source()+" "+msg.get_packet_id()+" "+msg.get_PIR()+" "+msg.get_vib()
							 +" "+msg.get_mic()+" "+msg.get_temp()+" "+msg.get_light()+" "+formatLogValue(1,(long)msg.get_watt())
							 +" "+formatLogValue(3,(long)msg.get_frequency())+" "+formatLogValue(4,(long)msg.get_rms_voltage())+" "+formatLogValue(5,(long)msg.get_rms_current())
							 +" "+formatLogValue(6,(long)msg.get_plogg_on_time())+" "+formatLogValue(7,(long)msg.get_reactive_power())
							 +" "+formatLogValue(9,(long)msg.get_phase_angle())+" "+formatLogValue(6,(long)msg.get_time_on()));
		  //System.out.println();
		  //System.out.flush();
	  }
	}
	catch (IOException e) {
	    System.err.println("Error on " + reader.getName() + ": " + e);
	}
    }
}

