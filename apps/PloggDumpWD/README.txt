- controllare che i timer non siano attivi
- implementare syncro

- add a field in the database with the time java process receive a packet (posso ricavare il tempo di generazione?) - uso lo stesso ma lo definisco diversamente, non come time stamp e passo il valore generato da java
- reset plogg before start to taking measurements
- mettere delay nello start del timer
- lancio un timer per tutte le operazioni, poi non appena ricevo i vari valori, memorizzo nel modo richiesto e decremento un contatore e quando ho tutte le metriche o un array con tutti 1, mando il pacchetto


MIGLIORAMENTI FUTURI
- plogg_time to remove because is not synch - chiedere il massimo per temp, light e mic -> se 12 bit per ognuno e 2 per i valori 0 e 1 ci sto dentro - per semplificare controllare il valore massimo di temp che posso ottenere con 8 bit - se il massimo e alto me la cavo con solo 8 bit cosi' uso tutti i 4 byte per queste metriche e l'altor per i valori 0 e 1 di PIR e VIB - piu' semplice da implementare - per la temperatura devo shiftare di 4 a destra, cosi' perdo i meno significativi e ho 8 bit poi quando ricevo shifto ancora a sinistra
- uso lo stesso formato di pacchetto, solo dopo la lettura del pacchetto energy, metto nei byte del tempo e in quello del packet value la somma di questi byte e faccio un po' piu' di lavoro quando lo estraggo dal java (vedere come ho fatto in alba e in perl per le latenze)
- ask for the same packet then substitute the value of plog_time with the value of temp and light and the value of value_code with the other three that are equal to 0 or 1
- then update the java stuff for the test and then the connection to the db and its structure
