
module PloggSensorTestC {

    uses {
        interface Boot;
        interface SplitControl as SubControl;
        interface SplitControl as DumpControl;
        interface SmartBox;
        
        //Dump interfaces
        interface WBDump;
        interface AMPacket;
    }
}

implementation {

    context_msg_t packet_payload;
    
    task void uartEchoTask() {
        if (call WBDump.put(10, AM_CONTEXT_MSG, sizeof(context_msg_t), &packet_payload) != SUCCESS)
            post uartEchoTask();
    }

    event void SmartBox.samplesReady(context_msg_t* payload) {
        memcpy(&packet_payload, payload, sizeof(context_msg_t));
        post uartEchoTask();
    } 	

    event void Boot.booted() {
        call SubControl.start();
    }
    
    event void SubControl.startDone(error_t error) {
        if ( error == SUCCESS ) {
            call DumpControl.start();
#ifdef PM_USE_LED 
            call SmartBox.ledOn();
#endif
        }
    }

    event void SubControl.stopDone(error_t error) {}
    
    event void DumpControl.startDone(error_t error) {}
    
    event void DumpControl.stopDone(error_t error) {}
    
}
