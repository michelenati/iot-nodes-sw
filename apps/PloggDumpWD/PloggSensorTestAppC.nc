#include "EnergyData.h"
#include "ContextData.h"

configuration PloggSensorTestAppC {}

implementation {
    components PloggSensorTestC;
    
    components MainC;
    PloggSensorTestC.Boot                 -> MainC;
    
    components SmartBoxC;
    PloggSensorTestC.SubControl           -> SmartBoxC;
    PloggSensorTestC.SmartBox             -> SmartBoxC;
        
    components ActiveMessageC;
    PloggSensorTestC.AMPacket             -> ActiveMessageC;
    
    //------------------------------------------------------
	// Serial Communication
	//------------------------------------------------------
	components WBDumpC;
	PloggSensorTestC.WBDump               -> WBDumpC;
	PloggSensorTestC.DumpControl          -> WBDumpC;
}
