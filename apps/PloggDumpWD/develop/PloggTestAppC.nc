#include "EnergyData.h"

configuration PloggTestAppC { }

implementation
{
    components MainC, NoLedsC, LedsC, PloggC, PloggTestC;

    PloggTestC.SplitControl     -> PloggC;
    PloggTestC.Boot             -> MainC.Boot;
    PloggTestC.Leds             -> LedsC;

    PloggTestC.EnergyMeter      -> PloggC;
    
    components new TimerMilliC();
    PloggTestC.TimerCommand     -> TimerMilliC;
    
    //------------------------------------------------------
	// Serial Communication
	//------------------------------------------------------
	components SerialActiveMessageC;
	components new SerialAMSenderC( AM_ENERGY_MSG ) as UartSender;
	components new SerialAMReceiverC( AM_ENERGY_MSG ) as UartReceiver;
	
	PloggTestC.UartControl      -> SerialActiveMessageC;
	PloggTestC.SendUart         -> UartSender;
	PloggTestC.ReceiveUart      -> UartReceiver;

}

