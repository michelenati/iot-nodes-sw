module PloggTestC
{
    uses {
        interface SplitControl;
        interface Boot;
        interface Leds;
        interface EnergyMeter;
        interface Timer<TMilli> as TimerCommand;
        
        //UART interfaces
        interface SplitControl as UartControl;
        interface AMSend as SendUart;
        interface Receive as ReceiveUart;
        
    }
}

implementation
{
    bool serialSending;                 //node is already sending messages over the UART
        
    message_t uart_msg_;                    //buffer for the message sent over the UART
    norace energy_msg_t * uart_payload_;    //pointer to the uart header of the message
    //MM_ctrl_msg_t * uart_recv_;           //pointer to the control header of the message received from the UART
 
    task void sendUart() {
        if( serialSending == FALSE ) {
            serialSending = TRUE;
            call SendUart.send( AM_BROADCAST_ADDR, &uart_msg_, sizeof( energy_msg_t ) );
        }
 	}
	
	event void UartControl.startDone( error_t result ) {}
    event void UartControl.stopDone( error_t result ) {}

    event void Boot.booted() {
        uart_payload_ = (energy_msg_t*) call SendUart.getPayload( & uart_msg_, sizeof( energy_msg_t ) );
        //start the UART module
        call UartControl.start();
    }
  
    async event void EnergyMeter.receiveDone( uint8_t* buf, uint8_t len, error_t error ) {
        
        if ( error == SUCCESS ) {
            //add control about the len
            if ( len == sizeof( energy_msg_t ) ) {
                memcpy( uart_payload_, (energy_msg_t*)buf, len );
                post sendUart();
                call Leds.led1On();
            }
        }
    }

    event void SplitControl.startDone(error_t error) {
        //call TimerCommand.startPeriodic( 4000 );
        call EnergyMeter.getPeriodicValues( 4000 );
    }

    event void SplitControl.stopDone(error_t error) {}
    
    event void TimerCommand.fired() {
        //call EnergyMeter.getLiveInfo();
    }
    
    /********** UART SECTION ***********/

    event void SendUart.sendDone( message_t * msg, error_t error ) {

        serialSending = FALSE;
        if ( error != SUCCESS )
            post sendUart();
        else call Leds.led1Off();
    }
    
    event message_t* ReceiveUart.receive(message_t* msg, void* payload, uint8_t len) {
        //uart_recv_ = payload;
        return msg;
     }
}
