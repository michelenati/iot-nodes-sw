#include "EnergyData.h"
#include "ContextData.h"

configuration PloggSensorTestAppC {}

implementation {
    components PloggSensorTestC;
    
    components MainC;
    PloggSensorTestC.Boot                 -> MainC;
    
    components LedsC;
    PloggSensorTestC.Leds                 -> LedsC;
    
    components new TimerMilliC();
    PloggSensorTestC.Timer                -> TimerMilliC;
    
    components ActiveMessageC;
    PloggSensorTestC.AMPacket             -> ActiveMessageC;
    
    //------------------------------------------------------
	// Plogg communication
	//------------------------------------------------------
    components PloggC;
    PloggSensorTestC.PloggControl         -> PloggC;
    PloggSensorTestC.EnergyMeter          -> PloggC;
    
    
    //------------------------------------------------------
	// Sensor board communication
	//------------------------------------------------------
    components new LightA500C() as MyLightSensor;
    components new TempA500C() as MyTempSensor;
    components new MicA500C() as MyMicSensor;
    components new VibrationA500C() as MyVibrationSensor;
    components new PIRA500C() as MyPIRSensor;
    PloggSensorTestC.MyLight -> MyLightSensor;
    PloggSensorTestC.MyTemp-> MyTempSensor;
    PloggSensorTestC.MyMic-> MyMicSensor;
    PloggSensorTestC.MyVibration-> MyVibrationSensor;
    PloggSensorTestC.MyPIR-> MyPIRSensor;
    
    //------------------------------------------------------
	// Serial Communication
	//------------------------------------------------------
	components WBDumpC;
	PloggSensorTestC.WBDump               -> WBDumpC;
}
