#!/bin/perl

my $max_id = shift;
my $image = shift;

for ($mote = 0; $mote<=$max_id; $mote++) {
    system("tos-set-symbols --objcopy msp430-objcopy --objdump msp430-objdump --target ihex $image $image.out-$mote TOS_NODE_ID=$mote ActiveMessageAddressC__addr=$mote");
}
