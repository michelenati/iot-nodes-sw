
module PloggSensorTestC {

    uses {
        interface Boot;
        interface SplitControl as PloggControl;
        interface SplitControl as DumpControl;
        
        interface Leds;
        interface Timer<TMilli>;
        
        interface EnergyMeter;
        interface Read<uint16_t> as MyLight;	  
        interface Read<uint16_t> as MyMic;	  
        interface Read<uint16_t> as MyTemp;	  
        interface Read<uint16_t> as MyVibration;	  
        interface Read<uint16_t> as MyPIR;
        
        //Dump interfaces
        interface WBDump;
        interface AMPacket;
    }
}

implementation {

    context_msg_t msg_to_dump; //buffer for the packet payload
    context_msg_t* packet_payload = &msg_to_dump;

    uint16_t counter;
    uint16_t packet_id;
    uint16_t sampling_interval;
    
    task void readLight() {
        call MyLight.read();   
    }
    
    task void nextSample() {
        memset(packet_payload, 0, sizeof(context_msg_t));
        post readLight();
    }
    
    task void uartEchoTask() {
        if (call WBDump.put(10, AM_CONTEXT_MSG, sizeof(context_msg_t), packet_payload) != SUCCESS)
            post uartEchoTask();
    }
 	
    task void sendReceivedMetrics() {
        packet_payload -> packet_id = packet_id;
        packet_payload -> source    = TOS_NODE_ID;
        call Timer.startOneShot( sampling_interval );
        post uartEchoTask();

        if (packet_id < 30000 )
            packet_id ++;
        else packet_id = 0;
    }

    event void Boot.booted() {
        call PloggControl.start();
        packet_id = 0;
    }
    
    event void PloggControl.startDone(error_t error) {
        if ( error == SUCCESS ) {
            //MN: maybe I should put here the start for the uart module (SplitControl interface required)
            call DumpControl.start();
            sampling_interval = 2 * CS_SENSING_PERIOD;
            call EnergyMeter.resetAccumulators();
            call Timer.startOneShot( sampling_interval );
        }
    }

    event void PloggControl.stopDone(error_t error) {}
    
    event void DumpControl.startDone(error_t error) {}
    
    event void DumpControl.stopDone(error_t error) {}
    
    event void MyLight.readDone(error_t result, uint16_t data){
       if ( result != SUCCESS ) post nextSample();
       packet_payload -> light = data;
       call MyTemp.read();
	}

	event void MyTemp.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   packet_payload -> temp = data;
	   call MyPIR.read();
	} 

    event void MyPIR.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   if ( data == SB_MAX_SENSOR_VALUE )
	       packet_payload -> PIR = 1;
	   else packet_payload -> PIR = 0;
	   call MyMic.read();
	}
	
	event void MyMic.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   packet_payload -> mic = data;
	   call MyVibration.read();
	}

	event void MyVibration.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   if ( data == SB_MAX_SENSOR_VALUE )
	       packet_payload -> vib = 1;
	   else packet_payload -> vib = 0;
#ifdef PM_PLOGG_CONNECTED
	   call EnergyMeter.getLiveInfo();
#else
	   post sendReceivedMetrics();
#endif
	}
    
    async event void EnergyMeter.receiveDone( uint8_t* buf, uint8_t len, error_t error ) {
        
        if ( error == SUCCESS ) {
            //add control about the len
            if ( len == sizeof( energy_msg_t ) ) {
                packet_payload -> watt = ((energy_msg_t*)buf) -> watt;
                packet_payload -> frequency = ((energy_msg_t*)buf) -> frequency;
                packet_payload -> rms_voltage = ((energy_msg_t*)buf) -> rms_voltage;
                packet_payload -> rms_current = ((energy_msg_t*)buf) -> rms_current;
                packet_payload -> plogg_on_time = ((energy_msg_t*)buf) -> plogg_on_time;
                packet_payload -> reactive_power = ((energy_msg_t*)buf) -> reactive_power;
                packet_payload -> phase_angle = ((energy_msg_t*)buf) -> phase_angle;
                packet_payload -> time_on = ((energy_msg_t*)buf) -> time_on;
                post sendReceivedMetrics();
            }
            call Leds.led1On();
        }
    }
        
    event void Timer.fired() {
        memset(packet_payload, 0, sizeof(context_msg_t));
        post readLight();
    }
}
