/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Interface for sending logs to the back end using WISEBED
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/


interface WBDump {

    /**
    * Enques the next message to send to the back end using WISEBED
    *
    * @param type The type of the message specified in WisebedMsg.h
    * @param am_id The AM_TYPE of the message
    * @param len The len of the message
    * @param payload The message to send
    * @return Return the result code of the function
    **/
    command error_t put(uint8_t type, uint8_t am_id, uint8_t len, void * payload);
    
    /**
    * Checks if the queue is flushing
    *
    * @return Return 1 if the queue is flushing messages to the back end
    **/
    command uint8_t isFlushing();
    
    /**
    * Starts flushing the queue
    *
    * @return Return the result code of the function
    **/
    async command error_t flush();
    
    /**
    * Stops flushing the queue
    *
    * @return Return the result code of the function
    **/
    async command error_t stopFlush();
}
