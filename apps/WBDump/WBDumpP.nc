/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Component implementing the WBDump interface
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/

#include "WisebedMsg.h"

module WBDumpP @safe() {
    provides {
        interface SplitControl;
        interface WBDump;
    }
    uses {
        interface Boot;
        interface SplitControl as SerialControl;
        
        interface AMSend as UartSend[am_id_t id];
        interface Packet as UartPacket;
        interface AMPacket as UartAMPacket;
        
        interface Queue<message_t*> as Queue;
        interface Pool<message_t> as Cache;
        
        interface Leds;
    }
}

implementation {

    enum {
        S_STOPPED,
        S_STARTED,
        S_FLUSHING,
    };
    
    message_t* msg_to_send;
    uint8_t am_msg_type;
    
    uint8_t state = S_STOPPED;
    
    task void retrySend() {
        if (call UartSend.send[am_msg_type](AM_BROADCAST_ADDR, msg_to_send, call UartPacket.payloadLength(msg_to_send)) != SUCCESS)
            post retrySend();
    }
    
    task void sendNext() {
        if(call Queue.size() > 0) {
            msg_to_send = call Queue.dequeue();
            if (call UartSend.send[am_msg_type](AM_BROADCAST_ADDR, msg_to_send, call UartPacket.payloadLength(msg_to_send)) != SUCCESS)
                post retrySend();
        } else state = S_STARTED;
    }

    event void Boot.booted() {
        call Leds.led1Toggle();
    }
    
    command error_t SplitControl.start() {
        return call SerialControl.start();
    }
    
    command error_t SplitControl.stop() {
        return call SerialControl.stop();
    }
        
    event void SerialControl.startDone(error_t error) {
        if (state == S_STOPPED) {
            atomic state = S_STARTED;
            signal SplitControl.startDone(error);
        }
    }
    
    event void SerialControl.stopDone(error_t error) {
        atomic state = S_STOPPED;
        signal SplitControl.stopDone(error);
    }
    
    event void UartSend.sendDone[am_id_t id](message_t* msg, error_t error) {
        if(error == SUCCESS) {
            call Cache.put( msg_to_send );
            if(call Queue.size() > 0)
                post sendNext();
            else state = S_STARTED;
        } else post retrySend();
    }
    
    command error_t WBDump.put(uint8_t type, uint8_t am_id, uint8_t len, void * payload) {
        message_t* new_msg;
        void* m;
        uint8_t result;
        uint8_t payload_len = len;
        am_msg_type = type;
                
        if ( ( call Queue.size() > WB_BUFFER_SIZE ) || ( payload_len > TOSH_DATA_LENGTH ) )
            return FAIL;
            
        new_msg = call Cache.get();
        
        //Cache is full - packet drop
        if ( new_msg == NULL ) {
            return FAIL;
        }
            
        m = call UartPacket.getPayload(new_msg, payload_len);
        call UartPacket.clear(new_msg);
        
        if ( len != 0 )
            memcpy( m, (uint8_t*)payload, len );

        call UartPacket.setPayloadLength(new_msg, payload_len);
    
        atomic {result = call Queue.enqueue( new_msg );}
        
        if((state == S_STARTED) && (call Queue.size() >= 1)) {
            state = S_FLUSHING;
            post sendNext();
        }

        return result;
    }
    
    command uint8_t WBDump.isFlushing() {
        if ( state == S_FLUSHING )
            return 1;
        else return 0;
    }
    
    async command error_t WBDump.flush() {
        atomic {
            if(state == S_FLUSHING)
                return SUCCESS;
            if(call Queue.empty())
                return FAIL;
            state = S_FLUSHING;
        }
        post sendNext();
        return SUCCESS;
  }
  
  async command error_t WBDump.stopFlush() {
        state = S_STARTED;
        return SUCCESS;
  }
}
