/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Module implementing the REDUCE collection SW
 *
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/


module PloggSensorTestC {

    uses {
        interface Boot;
        interface SplitControl as SubControl;
        interface SplitControl as DumpControl;
        interface SmartBox;
        
        //Dump interfaces
        interface WBDump;
        interface AMPacket;
    }
}

implementation {

    context_msg_t packet_payload;
    
    task void uartEchoTask() {
        if (call WBDump.put(10, AM_CONTEXT_MSG, sizeof(context_msg_t), &packet_payload) != SUCCESS)
            post uartEchoTask();
    }

    event void SmartBox.samplesReady(context_msg_t* payload) {
        memcpy(&packet_payload, payload, sizeof(context_msg_t));
        post uartEchoTask();
    } 	

    event void Boot.booted() {
        call SubControl.start();
    }
    
    event void SubControl.startDone(error_t error) {
        if ( error == SUCCESS ) {
            call DumpControl.start();
#ifdef PM_USE_LED 
            call SmartBox.ledOn();
#endif
        }
    }

    event void SubControl.stopDone(error_t error) {}
    
    event void DumpControl.startDone(error_t error) {}
    
    event void DumpControl.stopDone(error_t error) {}
    
}
