/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Component implementing the SmartBox interface
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/
 
module SmartBoxP {

    provides {
        interface SplitControl as SplitControl;
        interface SmartBox;
    }

    uses {
        interface Boot;
        interface SplitControl as PloggControl;
        
        interface Leds;
        interface Timer<TMilli>;
        interface Timer<TMilli> as CATimer;
        interface Random;
        
        interface EnergyMeter;
        interface Read<uint16_t> as MyLight;	  
        interface Read<uint16_t> as MyMic;	  
        interface Read<uint16_t> as MyTemp;	  
        interface Read<uint16_t> as MyVibration;	  
        interface Read<uint16_t> as MyPIR;
        interface LedsA500 as MyLed;
        
        interface Watchdog as WatchdogPlogg;
        
    }
}

implementation {

    context_msg_t msg_to_dump; //buffer for the packet payload
    context_msg_t* packet_payload = &msg_to_dump;

    bool     watchdog;
    uint8_t  num_samples;
    uint16_t packet_id;
    uint16_t sampling_interval;
    uint32_t mic;
    
    task void touchTheDog() {
        call WatchdogPlogg.touch();
        //call WatchdogPlogg.disable();
    }
    
    task void readLight() {
        call MyLight.read();   
    }
    
    task void nextSample() {
        memset(packet_payload, 0, sizeof(context_msg_t));
        post readLight();
    }
 	
    task void sendReceivedMetrics() {
        packet_payload -> packet_id = packet_id;
#ifdef PM_WISEBED_PKT
        packet_payload -> source    = TOS_NODE_ID;
#endif

        signal SmartBox.samplesReady(packet_payload);
        
        if (packet_id < 65535L )
            packet_id ++;
        else packet_id = 0;

        memset(packet_payload, 0, sizeof(context_msg_t));        
        call Timer.startOneShot( sampling_interval );
        call CATimer.startOneShot( 2 * CS_SENSING_PERIOD );
    }

    event void Boot.booted() {
        memset(packet_payload, 0, sizeof(context_msg_t));
        watchdog = FALSE;
        num_samples = 0;
        packet_id = 0;
        mic = 0;
    }
    
    command void SmartBox.ledToggle() {
        call MyLed.ledToggle();
    }
    
    command void SmartBox.ledOn() {
        call MyLed.ledOn();
    }
    
    command void SmartBox.ledOff() {
        call MyLed.ledOff();
    }
    
    command void SmartBox.enableOffTimers(uint8_t on) {
        call EnergyMeter.enableOffTimers( on );
    }
    
    command void SmartBox.setSamplingPeriod(uint16_t period) {
        sampling_interval = period;
    }
    
    command error_t SplitControl.start() {
        return call PloggControl.start();
    }
    
    command error_t SplitControl.stop() {
        return call PloggControl.stop();
    }
    
    event void PloggControl.startDone(error_t error) {
        uint16_t jitter;
        if ( error == SUCCESS ) {
            sampling_interval = PM_FLUSH_DELAY;
            jitter = call Random.rand16() % PM_FLUSH_JITTER;
            //smpling_interval = 4000;
            call EnergyMeter.resetAccumulators();
            call Timer.startOneShot( sampling_interval + jitter );
            call CATimer.startOneShot( 2 * CS_SENSING_PERIOD );
        }
        signal SplitControl.startDone(error);
    }

    event void PloggControl.stopDone(error_t error) { signal SplitControl.stopDone(error); }
        
    event void MyLight.readDone(error_t result, uint16_t data){
       if ( result != SUCCESS ) post nextSample();
       packet_payload -> light = data;
       call MyTemp.read();
	}

	event void MyTemp.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   packet_payload -> temp = data;
	   call MyPIR.read();
	} 

    event void MyPIR.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   if ( data == SB_MAX_SENSOR_VALUE )
	       packet_payload -> PIR += 1;
	   else packet_payload -> PIR += 0;
	   call MyMic.read();
	}
	
	event void MyMic.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   //packet_payload -> mic += data;
	   atomic mic += data;
	   call MyVibration.read();
	}

	event void MyVibration.readDone(error_t result, uint16_t data){
	   if ( result != SUCCESS ) post nextSample();
	   if ( ( data == SB_MAX_SENSOR_VALUE ) || ( packet_payload -> vib == 1 ) )
	       packet_payload -> vib = 1;
	   else packet_payload -> vib = 0;
	   atomic num_samples ++;
	   call CATimer.startOneShot( 2 * CS_SENSING_PERIOD );
	}
    
    async event void EnergyMeter.receiveDone( uint8_t* buf, uint8_t len, error_t error ) {
    
        post touchTheDog();
        
        if ( error == SUCCESS ) {
            //add control about the len
            if ( len == sizeof( energy_msg_t ) ) {
                packet_payload -> watt = ((energy_msg_t*)buf) -> watt;
                packet_payload -> frequency = ((energy_msg_t*)buf) -> frequency;
                packet_payload -> rms_voltage = ((energy_msg_t*)buf) -> rms_voltage;
                packet_payload -> rms_current = ((energy_msg_t*)buf) -> rms_current;
                packet_payload -> plogg_on_time = ((energy_msg_t*)buf) -> plogg_on_time;
                packet_payload -> reactive_power = ((energy_msg_t*)buf) -> reactive_power;
                packet_payload -> phase_angle = ((energy_msg_t*)buf) -> phase_angle;
                packet_payload -> time_on = ((energy_msg_t*)buf) -> time_on;
                post sendReceivedMetrics();
            }
            call Leds.led1On();
        }
    }
        
    event void Timer.fired() {
        call CATimer.stop();
        atomic {
            if ( num_samples > 0 ) packet_payload -> mic = ( mic / num_samples );
            num_samples = 0;
            mic = 0;
        }
        
#ifdef PM_PLOGG_CONNECTED
        if (watchdog == FALSE) {
            watchdog = TRUE;
            call WatchdogPlogg.enable( 20000 );
        }
        call EnergyMeter.getLiveInfo();
#else
        post sendReceivedMetrics();
#endif
    }
    
    event void CATimer.fired() {
        post readLight();
    }
    
    event void WatchdogPlogg.fired(bool guilty) {
        // currently not implemented
    }
}

