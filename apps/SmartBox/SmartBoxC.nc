/*
 * Copyright (c) 2012 Centre for Communication Systems Research,
 * University of Surrey, Guildford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of Surrey nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * ARCHED ROCK OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */

/**
 * Component implementing the SmartBox interface
 * 
 * @author Michele Nati <M.Nati@surrey.ac.uk>
 * @version $Revision$ $Date$
 **/

#include "EnergyData.h"
#include "ContextData.h"

configuration SmartBoxC {

    provides {
        interface SplitControl;
        interface SmartBox;
    }
}

implementation {
    components SmartBoxP;
    
    components MainC;
    SmartBoxP.Boot                 -> MainC;
    
    SplitControl                   = SmartBoxP;
    SmartBox                       = SmartBoxP;
    
    components LedsC;
    SmartBoxP.Leds                 -> LedsC;
    
    components RandomC;
	SmartBoxP.Random               -> RandomC;
    
    components new TimerMilliC() as Timer;
    SmartBoxP.Timer                -> Timer;
    
    components new TimerMilliC() as CATimer;
    SmartBoxP.CATimer              -> CATimer;
    
    //------------------------------------------------------
	// Plogg communication
	//------------------------------------------------------
    components PloggC;
    SmartBoxP.PloggControl         -> PloggC;
    SmartBoxP.EnergyMeter          -> PloggC;
    
    
    //------------------------------------------------------
	// Sensor board communication
	//------------------------------------------------------
    components new LightA500C() as MyLightSensor;
    components new TempA500C() as MyTempSensor;
    components new MicA500C() as MyMicSensor;
    components new VibrationA500C() as MyVibrationSensor;
    components new PIRA500C() as MyPIRSensor;
    components LedsA500C as MyLed;
    SmartBoxP.MyLight -> MyLightSensor;
    SmartBoxP.MyTemp-> MyTempSensor;
    SmartBoxP.MyMic-> MyMicSensor;
    SmartBoxP.MyVibration-> MyVibrationSensor;
    SmartBoxP.MyPIR-> MyPIRSensor;
    SmartBoxP.MyLed-> MyLed;
    
        
    //------------------------------------------------------
	// Watchdog for monitoring Plogg interaction
	//------------------------------------------------------
    components new WatchdogC() as WatchdogPloggC;
    SmartBoxP.WatchdogPlogg -> WatchdogPloggC;

}
