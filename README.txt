***********
* INSTALL *
***********

In order to install the required SW, please:

- copy the tos folder content in the indicated subdirectories of your tinyos distribution ($TOSROOT/tos/)
- copy the apps folder content in the apps directory of your tinyos distribution ($TOSROOT/apps/)

***********
*** RUN ***
***********

In order to run the example application

- go in $TOSROOT/apps/PloggDumpWD
- compile and install the application on the testing Plogg using the following command

make telosb install.X

where X is the ID you wish to assign to your Plogg.


***********
** TEST ***
***********

In order to test data are correctly generated run the following:

cd $TOSROOT/apps/PloggMeter/interfaces

java ContextDumpWB -comm serial@device_port:telosb

where device port should be replaced by the output of motelist command (device column):

Reference  Device                      Description
---------- --------------------------- ---------------------------------------
XBTE0VIK   /dev/tty.usbserial-XBTE0VIK (none)

usually on a linux machine with only one device connected, this should be:
/dev/ttyUSB0

Output should resemble something like the following:
14:13:326	Wed May 13 17:14:13 BST 2015 176 620 0 0 0 0 0 0.00 W  50.0 Hz  238.2 V  0.000 A  0 days 00:11.01 0.00 VAR  0 Degrees  0 days 00:00.00
14:13:971	Wed May 13 17:14:13 BST 2015 176 621 0 0 0 0 0 0.00 W  50.0 Hz  238.2 V  0.000 A  0 days 00:11.02 0.00 VAR  0 Degrees  0 days 00:00.00
14:15:21	Wed May 13 17:14:15 BST 2015 176 622 0 0 0 0 0 0.00 W  50.0 Hz  238.1 V  0.000 A  0 days 00:11.03 0.00 VAR  0 Degrees  0 days 00:00.00
14:16:74	Wed May 13 17:14:16 BST 2015 176 623 0 0 0 0 0 0.00 W  50.0 Hz  238.1 V  0.000 A  0 days 00:11.04 0.00 VAR  0 Degrees  0 days 00:00.00
14:17:267	Wed May 13 17:14:17 BST 2015 176 624 0 0 0 0 0 0.00 W  50.0 Hz  238.1 V  0.000 A  0 days 00:11.05 0.00 VAR  0 Degrees  0 days 00:00.00

Columns are the following:
- timestamp when packet is read: 	mm:ss:ms
- time stamp when packet is generated: 	weekday, month, day, timestamp timezone year
- node_id:
- packet_id:
- PIR:					0/1 for each time a presence is detected (sampling frequency every 3 seconds)
- vib:					0/1 if a vibration is detected
- mic:					output of the microphone sensor
- temp:					output of the temperature sensor. To convert use this formula: 5.3*0.01*temp-40.4
- light:				output of the light sensor
- watts:				aggregate power of the connected devices
- frequency:				frequency of the connected devices
- voltage:				voltage of the connected devices
- current:				current of the connected devices
- days_plogg_is_on:			number of days, hours
- days_devices_are_connected:		number of days, hours

**********
*MAKEFILE*
**********

It can be useful to adjust the value of the following constant

PM_FLUSH_JITTER used in case of many Plogg sending data at the same time -> add a jitter for desynch
PM_FLUSH_DELAY interval for periodically generating new readings from the Plogg

(trick: in order to get proper synchronization between the Telosb and the Plogg when powering them 
the right procedure is the following:
- connect the USB
- plug within 8 seconds the Plogg into the power socket)